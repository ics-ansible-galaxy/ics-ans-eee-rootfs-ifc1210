ics-ans-eee-rootfs-ifc1210
==========================

Ansible playbook to install an EEE rootfs boot server for VME IOCs.

Requirements
------------

- ansible >= 2.4
- molecule >= 2.6

License
-------

BSD 2-clause
